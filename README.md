This is an empty nginx/php7 front controller site.
=======================================================

TLDR: Usage
=============
Simply clone this repo, then move the script make_new_localhost_site somewhere in your path ... Then you could simply run:
```shell
make_new_localhost_site  alabala_site | bash
```

Detailed manual usage example:
======================================
Suppose that you want a new site, that will be available at http://new_site_name/ , and that will be served from your local PC.

1. Clone this repo:
```shell
git clone git@gitlab.com:delian66/empty_nginx_site_with_php7_front_controller.git /websites/new_site_name
```

2. Go inside /websites/new_site_name/ .
```shell
cd /websites/new_site_name/
```

3. You should edit /websites/new_site_name/nginx.conf and customize the server_name inside nginx.conf:
```shell
sed -i s/empty_site/new_site_name/gm nginx.conf
```


4. Symlink the file /websites/new_site_name/nginx.conf to your /etc/nginx/sites-enabled/ folder.
```shell
sudo ln -s /websites/new_site_name/nginx.conf    /etc/nginx/sites-enabled/new_site_name
```

5. Restart nginx.
```shell
systemctl restart nginx.service
```


6. Edit your /etc/hosts file, and add the new site name in it, like this:
```shell
echo '127.0.0.1 new_site_name' | sudo tee -a /etc/hosts
```

=> visit the site at http://new_site_name/
```shell
curl -v http://new_site_name/
```


Post customization actions:
========================================
If everything is loading and working as you expected, you will probably want to cut the link to this git repo:

* Delete .git/:
```shell
rm -rf /websites/new_site_name/.git/
```

* Run:
```shell
git init . ; git add . ; git commit -m "Initialization." ;
```


